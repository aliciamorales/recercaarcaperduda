package edu.ub.recercaarcaperduda.actors;

import edu.ub.recercaarcaperduda.Colisio;
import edu.ub.recercaarcaperduda.Context;
import edu.ub.recercaarcaperduda.Util;

import java.awt.*;

/**
 * Forat Negre, traslada a nuestro heroe a otra Cambran
 */
public class ForatNegre extends AbstractActor {

    private static final Rectangle DIMENSIONS_ICONA = new Rectangle(0, 0, 52, 52);
    private Image icona;
    private boolean canviarCambra = false;


    public ForatNegre() {
        init();
    }

    public void tractarColisio(Colisio colisio) {
        Actor actor = colisio.getActor();
        if (actor instanceof Arqueologa) {
            canviarCambra = true;
        }
    }

    public Rectangle getLimits() {
        return new Rectangle(getX(), getY(), DIMENSIONS_ICONA.width,
                DIMENSIONS_ICONA.height);
    }

    public void render(Graphics2D g) {
        g.drawImage(icona, getX(), getY(), observer);
    }

    public void actualitzar(Context context) {
        if (canviarCambra) {
            ((Arqueologa) context.getJoc().getArqueologa()).randomTeleport(context);
            canviarCambra = false;
        }
    }

    // private methods *********************************************************

    private void init() {
        icona = Util.carregarImatge("blackhole.png",
                DIMENSIONS_ICONA.x, DIMENSIONS_ICONA.y,
                DIMENSIONS_ICONA.width, DIMENSIONS_ICONA.height);
    }

    private void incrementarForca(Arqueologa Arqueologa, int Forca) {
        Arqueologa.setForca(Arqueologa.getForca() + Forca);
    }

}
