package edu.ub.recercaarcaperduda.actors;

import edu.ub.recercaarcaperduda.*;

import java.awt.*;

/**
 * Representa el nostre Arqueologa. L'Arqueologa mante una llista amb els claus de Cofre trobats
 */
public class Arqueologa extends AbstractActor {
    public static final int AMPLADA = 40;
    public static final int ALCADA = 71;
    private static final int FRAMES_CHANGE = 1;
    private static final float DANY_PER_SEGON = 1.0f;
    private static final int numArmes = 3;
    int lastImage = 0;
    private Image[] imatges;
    private boolean bPosChanged = false;
    private int nFramesToChange = FRAMES_CHANGE;
    private int direccio = -1;
    private int diamantsCount[];
    private double[] armes;

    private int numClaus = 0;
    private int[][] claus;
    private boolean arcaOberta = false;
    private boolean coronelTrobat = false;
    private int[] combinacio;

    /**
     * Constructor.
     */
    public Arqueologa() {
        init();
    }

    public void inicialitzar() {
        super.inicialitzar();
        arcaOberta = false;
        coronelTrobat = false;
        //preestablim la combinacio
        combinacio = new int[4];
        combinacio[0] = 1;
        combinacio[1] = 2;
        combinacio[2] = 3;
        combinacio[3] = 4;
        numClaus = 10;
        diamantsCount = new int[4]; //comptador de diamants de cada color
        claus = new int[10][4];
        armes = new double[]{5.,5.,5.};
    }

    public boolean haTrobatCoronel() {
        return coronelTrobat;
    }

    public void setHaTrobatCoronel(boolean b) {
        this.coronelTrobat = b;

    }

    public void lluitarCoronel(double[] cops) {
        double[] copsArqueologa = armes;

        for (int i = 0; i < cops.length; i++) {
            //el Arqueologa es defensa
            System.out.println("Cop cap " + i + ":" + cops[i] + " Cop Arqueologa: " + copsArqueologa[i]);
            if (cops[i] > copsArqueologa[i])
                this.forca -= cops[i];
        }

        if (this.forca < 0) this.forca = 0;
    }

    public void setHaTrobatArcaAlianca(boolean b) {
        this.arcaOberta = b;

    }

    public boolean haObertArcaPerduda() {
        return arcaOberta;
    }

    public void obrirArcaPerduda() {
        if(claus.length>0){
            for( int[] combinacioArca : claus){
                boolean trobat = true;
                float quitaForca = 25;
                for (int i = 0; i < combinacioArca.length && !trobat; i++) {
                    System.out.println("Combinacio arca " + i + ":" + combinacioArca[i] + " Combinacio Arqueologa: " + combinacio[i]);
                    if (combinacioArca[i] != combinacio[i]) {
                        this.forca -= quitaForca;
                        trobat = false;
                        arcaOberta = trobat;
                    }
                }
            }
        }
    }

    public void addClau(int[] clau) {

        System.out.println("L'Arqueologa a trobat una Cofre amb una clau amb combinacio: " + clau[0]+ clau[1]+clau[2]+clau[3]);

        if (numClaus < claus.length)
            claus[numClaus++] = clau;
    }
    public void addArma(Arma arma) {
        System.out.println("L'Arqueologa a trobat un Arma amb una potencia: " + arma.getForca());
        double min = armes [0];
        for (double a: armes){
            if(a<min){
                min = a;
            }
        }
        int pos = 0;
        if(arma.getForca()>min){
            int p = 0;
            for (double a: armes){
                if(a==min){
                    pos = p;
                }
                p++;
            }
            armes[pos] = arma.getForca();
        }
    }
    public void addDiamant(Diamant diamant) {

        System.out.println("El Arqueologa a trobat un Diamant de Color: " + diamant.getColor());

        switch(diamant.getColor()){
            case VERMELL:
                if(diamantsCount[0]>Constants.QUANTITAT_DIAMANTS_VERMELLS_TARONJA)
                    diamantsCount[0] ++;
                break;
            case BLAU:
                if(diamantsCount[0]>Constants.QUANTITAT_DIAMANTS_VERD_BLAU)
                    diamantsCount[1]++;
                break;
            case VERD:
                if(diamantsCount[0]>Constants.QUANTITAT_DIAMANTS_VERD_BLAU)
                    diamantsCount[2]++;
                break;
            case GROC:
                if(diamantsCount[0]>Constants.QUANTITAT_DIAMANTS_VERD_BLAU)
                    diamantsCount[3]++;
                break;
            case TARONJA:
                if(diamantsCount[0]>Constants.QUANTITAT_DIAMANTS_VERMELLS_TARONJA)
                    diamantsCount[4]++;
                break;
        }
    }

    public int getNumClaus() {
        return numClaus;
    }

    public boolean haTrobatElsDiamants() {
        int suma = 0;
        for (int i = 0; i < 4; i++)
            suma += diamantsCount[i];

        return suma >= Constants.QUANTITAT_DIAMANTS;
    }

    /**
     * Obte un rectangle ambs el limits de l'Arqueologa.
     *
     * @return els limits, x,y, amplada i alcada
     */
    public Rectangle getLimits() {
        return new Rectangle(x, y, AMPLADA, ALCADA);
    }

    public void actualitzar(Context ctx) {
        calcularNivellForca(ctx);
        int desX = 0;
        int desY = 0;
        bPosChanged = false;

        if (ctx.isKeyPressed(Context.KEY_DOWN)) {
            desY = 1;
            direccio = Constants.DIRECCIO_SUD;
            bPosChanged = true;
        } else if (ctx.isKeyPressed(Context.KEY_UP)) {
            desY = -1;
            direccio = Constants.DIRECCIO_NORD;
            bPosChanged = true;
        } else if (ctx.isKeyPressed(Context.KEY_LEFT)) {
            desX = -1;
            direccio = Constants.DIRECCIO_OEST;
            bPosChanged = true;
        } else if (ctx.isKeyPressed(Context.KEY_RIGHT)) {
            desX = 1;
            direccio = Constants.DIRECCIO_EST;
            bPosChanged = true;
        }

        int deltaX = 10;
        int auxX = x + (int) (deltaX * desX);
        int deltaY = 8;
        int auxY = y + (int) (deltaY * desY);

        Porta porta = testPorta(ctx, auxX, auxY);
        if (porta != null && porta.getNumNivellDesti() != -1 &&
                porta.getNumCambraDesti() != -1) {

            Temple temple = ctx.getJoc().getTemple();
            temple.setNivell(porta.getNumNivellDesti());
            temple.setNumCambra(porta.getNumCambraDesti());
            int[] posicio = porta.getPosicioCambraDesti();
            if (posicio != null) {
                x = posicio[0];
                y = posicio[1];
            }
        } else if (!testMur(ctx, auxX, auxY)) {
            x = auxX;
            y = auxY;
        }
    }

    public void tractarColisio(Colisio colisio) {
    }

    public void render(Graphics2D g) {
        int nImg = 0;
        if (bPosChanged) {
            nFramesToChange--;
            if (nFramesToChange == 0) {
                nFramesToChange = FRAMES_CHANGE;
                lastImage = (lastImage == 1) ? 0 : 1;
            }
        }

        if (direccio == Constants.DIRECCIO_NORD) {
            nImg = 2 + lastImage;
        } else {
            nImg = lastImage;
        }
        //Image image = imatges[nImg];
        Image image = imatges[0];
        g.drawImage(image, x, y, observer);
    }

    // private methods *********************************************************

    private void init() {
        imatges = new Image[1];
        for (int i = 0; i < imatges.length; i++)
            imatges[i] = Util.carregarImatge("cindy_low.png");
    }

    private void calcularNivellForca(Context ctx) {
        long t = ctx.getTempsFrame();
        float dany = DANY_PER_SEGON * t / 1000.f;
        setForca(Math.max(0, getForca() - dany));
    }

    public void randomTeleport(Context context) {
        Temple temple = context.getJoc().getTemple();
        Actor arqueologa = context.getJoc().getArqueologa();
        Cambra novaCambra = null;
        Cambra h = context.getCambra();
        Cambra[] cambres = temple.getCambres(temple.getNivell());

        if (cambres.length > 1) {
            boolean trobada = false;
            int fila = 0;
            int col = 0;
            int numCambra = 0;
            int[] posicio = null;

            while (!trobada) {
                numCambra = (int) (Math.random() * cambres.length);
                while (h == cambres[numCambra]) {
                    numCambra = (int) (Math.random() * cambres.length);
                }
                novaCambra = temple.getCambra(temple.getNivell(), numCambra);
                boolean lliure = false;
                while (!lliure) {
                    fila = (int) Math.max(0, (Math.random() * Constants.NUM_CELES_VERTICALS - 2));
                    col = (int) Math.max(0, (Math.random() * Constants.NUM_CELES_HORIZONTALS - 2));
                    char c1 = novaCambra.getValor(fila, col);
                    char c2 = novaCambra.getValor(fila + 1, col);
                    char c3 = novaCambra.getValor(fila, col + 1);
                    char c4 = novaCambra.getValor(fila + 1, col + 1);
                    lliure = (c1 == Constants.SIMBOL_TERRA &&
                            c2 == Constants.SIMBOL_TERRA &&
                            c3 == Constants.SIMBOL_TERRA &&
                            c4 == Constants.SIMBOL_TERRA);

                    // comprovar que cap actor esta dins la cela
                    Actor[] actors = novaCambra.getActorsAsArray();
                    int i = 0;

                    int cela[] = null;
                    while (i < actors.length && lliure) {
                        cela = novaCambra.getCela(actors[i].getPosicioInicial()[0],
                                actors[i].getPosicioInicial()[1]);
                        lliure = fila != cela[0] || col != cela[1];
                        i++;
                    }
                }

                posicio = novaCambra.getPosicioCela(fila, col);
                trobada = !testMur(context, posicio[0], posicio[1], arqueologa.getLimits());
            }
            temple.setNumCambra(numCambra);
            arqueologa.setPosicio(posicio[0], posicio[1]);
        }
    }


}
