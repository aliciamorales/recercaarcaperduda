package edu.ub.recercaarcaperduda.actors;

import edu.ub.recercaarcaperduda.*;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Soldats que vigilen la Temple, decrementan la forca de GuiJoc
 */
public class Soldat extends AbstractActor {


    private static final int INCREMENT_POSX = 1;
    private static final int INCREMENT_POSY = 1;
    private static final int[][] DIRECCIONS = {
            {1, 0},   // EST
            {-1, 0},  // OEST
            {0, 1},   // SUD
            {0, -1}   // NORD
    };
    private static final Rectangle DIMENSIONS_ICONA = new Rectangle(0, 0, 60, 30);
    private Image[] icones;
    private int[] direccio = DIRECCIONS[0];
    private int targetX = 0;
    private int targetY = 0;
    private boolean hasTarget = false;
    private boolean aturat = false;

    public Soldat(int forca) {
        this.forca = forca;
        init();
    }

    /**
     * Sobreescriu el metode per canviar de direccio cada cop que xoca amb un
     * mur.
     *
     * @param context
     */
    public void actualitzar(Context context) {

        if (isAturat()) return;

        Rectangle limits = new Rectangle(getX(), getY(), DIMENSIONS_ICONA.width, DIMENSIONS_ICONA.height);

        //establece el target si no lo tiene o lo ha alcanzado
        if (!hasTarget || hasReachedTarget(limits, targetX, targetY)) {
            targetX = context.getJoc().getArqueologa().getPosicio()[0];
            targetY = context.getJoc().getArqueologa().getPosicio()[1];
            hasTarget = true;
        }

        int posX = getX();
        int posY = getY();

        //de esta manera no se quedan totalmente parados cuando no pueden avanzar en diagonal
        int aleatori = (int) (Math.random() * 2);

        if (aleatori == 0) {
            if (getX() > targetX)
                posX -= INCREMENT_POSX;
            else if (getX() < targetX)
                posX += INCREMENT_POSX;
            else if (getY() > targetY)
                posY -= INCREMENT_POSY;
            else if (getY() < targetY)
                posY += INCREMENT_POSY;
        } else {
            if (getY() > targetY)
                posY -= INCREMENT_POSY;
            else if (getY() < targetY)
                posY += INCREMENT_POSY;
            else if (getX() > targetX)
                posX -= INCREMENT_POSX;
            else if (getX() < targetX)
                posX += INCREMENT_POSX;
        }


        Cambra h = context.getCambra();
        limits = new Rectangle(posX, posY, DIMENSIONS_ICONA.width, DIMENSIONS_ICONA.height);

        if (getXocaContraMurs(limits, h)) {
            hasTarget = false;
        } else if (!getXocaContraMurs(limits, h)) {
            setPosicio(posX, posY);
        }

    }

    public boolean hasReachedTarget(Rectangle limits, int targetX, int targetY) {
        return limits.getX() <= targetX && limits.getX() + limits.width >= targetX &&
                limits.getY() <= targetY && limits.getY() + limits.height >= targetY;
    }

    public Rectangle getLimits() {
        return new Rectangle(getX(), getY(), DIMENSIONS_ICONA.width, DIMENSIONS_ICONA.height);
    }

    public void tractarColisio(Colisio colisio) {
        Actor actor = colisio.getActor();
        if (actor instanceof Arqueologa) {
            Arqueologa arqueologa = (Arqueologa) actor;
            arqueologa.setForca(actor.getForca() - (forca/30));
        }
    }

    public void render(Graphics2D g) {
        int currentIcona = 0;
        g.drawImage(icones[currentIcona], getX(), getY(), observer);
    }

    // private methods *********************************************************

    private void init() {
        icones = new Image[2];
        BufferedImage iTmp = Util.carregarImatge("soldat.png");
        icones[0] = Util.flipImatgeHor(iTmp);
        icones[1] = iTmp;

    }

    private boolean getXocaContraMurs(Rectangle limits, Cambra h) {
        boolean xoca = false;
        int[][] celes = h.getCelesIntersectades(limits);
        int i = 0;
        while (i < celes.length && !xoca) {
            if (h.getValor(celes[i][0], celes[i][1]) == Constants.SIMBOL_MUR)
                xoca = true;
            i++;
        }
        return xoca;
    }

    public boolean isAturat() {
        return this.aturat;
    }

    public void setAturat(boolean aturat) {
        this.aturat = aturat;
    }

}
