package edu.ub.recercaarcaperduda.actors;

import edu.ub.recercaarcaperduda.*;

import java.awt.*;

/**
 * Cap del Morc, s'ha de vencer per completar la missio
 */
public class Coronel extends AbstractActor {

    private static final int AMPLADA = 70;
    private static final int ALCADA = 72;
    private Image image;



    public Coronel(float forca) {
        init();
        this.forca = forca;
    }

    public void actualitzar(Context context) {
    }

    public Rectangle getLimits() {
        return new Rectangle(x, y, AMPLADA, ALCADA);
    }

    public void tractarColisio(Colisio colisio) {
        Actor actor = colisio.getActor();
        if (actor instanceof Arqueologa) {
            Arqueologa arqueologa = (Arqueologa) actor;
            arqueologa.setHaTrobatCoronel(true);
            this.forca -= arqueologa.getForca();

            double[] cops = generarCops();
            arqueologa.lluitarCoronel(cops);

            setEstat(Constants.ESTAT_INACTIU);
        }
    }

    public double[] generarCops() {
        double[] cops = new double[3];
        for (int i = 0; i < cops.length; i++)
            cops[i] = Math.random() * forca;
        return cops;
    }

    public void render(Graphics2D g) {
        if (getEstat() == Constants.ESTAT_ACTIU) {
            g.drawImage(image, x, y, observer);
        }
    }

    private void init() {
        image = Util.carregarImatge("coronel.png");
    }
}
