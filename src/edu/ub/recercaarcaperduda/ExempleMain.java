package edu.ub.recercaarcaperduda;

import edu.ub.recercaarcaperduda.actors.*;

import static edu.ub.recercaarcaperduda.Constants.*;

/**
 * Demo.
 */
public class ExempleMain {

    private static final int MAX_DIAMANTS_PER_CAMBRA = 2;
    private static final int MAX_SOLDATS_PER_CAMBRA = 2;
    private static final int CORONEL_FORCA = 30;
    private static final int NUM_DIAMANTS = 6;
    private static final int NUM_COFRES = 10;
    private static final int NUM_ARMES = 10;
    private Joc joc;
    private Temple temple;
    private Arqueologa cindy_jones;

    private ExempleMain() {

        temple = new Temple(1, 3);
        temple.addCambra(0, 0, crearCambra0Nivell0());
        temple.addCambra(0, 1, crearCambra1Nivell0());
        temple.addCambra(0, 2, crearCambra2Nivell0());

        cindy_jones = new Arqueologa();
        Cambra cam = temple.getCambra(0, 0);
        int[] p = cam.getPosicioCela(10, 10);
        cindy_jones.setPosicioInicial(p[0], p[1]);

        distribuirDiamants();
        distribuirSoldats();
        distribuirCofres();
        posicionarCoronel();
        posicionarArmes();

        //initialitzacio del joc
        joc = new Joc(temple, cindy_jones);
        GuiJoc gui = new GuiJoc(joc);
    }

    /**
     * Principal.
     *
     * @param args
     */
    public static void main(String[] args) {
        new ExempleMain();
    }

    private Cambra crearCambra0Nivell0() {
        Cambra cam = Util.carregarCambra("c0_0.txt");

        Porta porta = cam.getPorta(14, 24);
        porta.setNumNivellDesti(0);
        porta.setNumCambraDesti(1);
        porta.setPosicioCambraDesti(cam.getPosicioCela(1, 1));

        porta = cam.getPorta(0, 10);
        porta.setNumNivellDesti(0);
        porta.setNumCambraDesti(2);
        porta.setPosicioCambraDesti(cam.getPosicioCela(14, 10));

        return cam;
    }

    private Cambra crearCambra1Nivell0() {
        Cambra h = Util.carregarCambra("c0_1.txt");

        Porta porta = h.getPorta(1, 0);
        porta.setNumNivellDesti(0);
        porta.setNumCambraDesti(0);
        porta.setPosicioCambraDesti(h.getPosicioCela(13, 22));


        return h;
    }

    private Cambra crearCambra2Nivell0() {
        Cambra h = Util.carregarCambra("c0_2.txt");
        Porta porta = h.getPorta(16, 10);
        porta.setNumNivellDesti(0);
        porta.setNumCambraDesti(0);
        porta.setPosicioCambraDesti(h.getPosicioCela(1, 10));

        return h;
    }

    private void distribuirDiamants() {
        int nivell;
        int numCambra;
        Cambra cam;
        for(int h = 0; h < QUANTITAT_DIAMANTS_VERMELLS_TARONJA; h++) {
            nivell = (int) (Math.random() * temple.getNumNivells());
            numCambra = (int) (Math.random() * temple.getNumCambres(nivell));
            cam = temple.getCambra(nivell, numCambra);
            Diamant v = new Diamant(COLORS_DIAMANTS.TARONJA);
            int[] cela = obtenirCelaLliure(cam);
            int[] posicio = cam.getPosicioCela(cela[0], cela[1]);
            v.setPosicioInicial(posicio[0], posicio[1]);
            cam.addActor(v);
            nivell = (int) (Math.random() * temple.getNumNivells());
            numCambra = (int) (Math.random() * temple.getNumCambres(nivell));
            cam = temple.getCambra(nivell, numCambra);
            Diamant t = new Diamant(COLORS_DIAMANTS.VERMELL);
            cela = obtenirCelaLliure(cam);
            posicio = cam.getPosicioCela(cela[0], cela[1]);
            t.setPosicioInicial(posicio[0], posicio[1]);
            cam.addActor(t);
        }
        for(int h = 0; h < QUANTITAT_DIAMANTS_VERD_BLAU; h++) {
            nivell = (int) (Math.random() * temple.getNumNivells());
            numCambra = (int) (Math.random() * temple.getNumCambres(nivell));
            cam = temple.getCambra(nivell, numCambra);
            Diamant v = new Diamant(COLORS_DIAMANTS.VERD);
            int[] cela = obtenirCelaLliure(cam);
            int[] posicio = cam.getPosicioCela(cela[0], cela[1]);
            v.setPosicioInicial(posicio[0], posicio[1]);
            cam.addActor(v);
            nivell = (int) (Math.random() * temple.getNumNivells());
            numCambra = (int) (Math.random() * temple.getNumCambres(nivell));
            cam = temple.getCambra(nivell, numCambra);
            Diamant t = new Diamant(COLORS_DIAMANTS.BLAU);
            cela = obtenirCelaLliure(cam);
            posicio = cam.getPosicioCela(cela[0], cela[1]);
            t.setPosicioInicial(posicio[0], posicio[1]);
            cam.addActor(t);
            nivell = (int) (Math.random() * temple.getNumNivells());
            numCambra = (int) (Math.random() * temple.getNumCambres(nivell));
            cam = temple.getCambra(nivell, numCambra);
            Diamant g = new Diamant(COLORS_DIAMANTS.GROC);
            cela = obtenirCelaLliure(cam);
            posicio = cam.getPosicioCela(cela[0], cela[1]);
            g.setPosicioInicial(posicio[0], posicio[1]);
            cam.addActor(g);
        }
    }

    private void distribuirSoldats() {
        int[] dades = {8, 2, 1}; //forca dels soldats: comandant, capita, soldat ras

        for (int i = 0; i < temple.getNumNivells(); i++) {

            for (int j = 0; j < temple.getNumCambres(i); j++) {
                Cambra cam = temple.getCambra(i, j);
                int numSoldats = (int) (Math.random() * (MAX_SOLDATS_PER_CAMBRA) + 1);

                for (int k = 0; k < numSoldats; k++) {
                    int[] cela = obtenirCelaLliure(cam);
                    int quantitat = dades[(int)(Math.random() * 3)];
                    Soldat s = new Soldat(quantitat);

                    int[] posicio = cam.getPosicioCela(cela[0], cela[1]);
                    s.setPosicioInicial(posicio[0], posicio[1]);
                    cam.addActor(s);
                }
            }
        }
    }

    private void distribuirCofres() {
        int nivell = 0;
        int numCambra = 0;
        int[] cela = null;
        Cambra cam = null;

        for (int numCofresAfegits = 0; numCofresAfegits < NUM_COFRES; numCofresAfegits++) {
            nivell = (int) (Math.random() * temple.getNumNivells());
            numCambra = (int) (Math.random() * temple.getNumCambres(nivell));
            cam = temple.getCambra(nivell, numCambra);
            cela = obtenirCelaLliure(cam);

            int contingut = (int) (Math.random() * (3));
            Cofre cofre = new Cofre(contingut);
            int[] posicio = cam.getPosicioCela(cela[0], cela[1]);
            cofre.setPosicioInicial(posicio[0], posicio[1]);
            cam.addActor(cofre);
        }
    }

    private void posicionarCoronel() {
        int nivell = (int) (Math.random() * temple.getNumNivells());
        int numCambra = (int) (Math.random() * temple.getNumCambres(nivell));
        Cambra cambra = temple.getCambra(nivell, numCambra);
        int[] cela = obtenirCelaLliure(cambra);
        int[] posicio = cambra.getPosicioCela(cela[0], cela[1]);
        Coronel Coronel = new Coronel((float)Math.random() * 100 );
        Coronel.setPosicioInicial(posicio[0], posicio[1]);
        cambra.addActor(Coronel);
    }

    private void posicionarArmes() {
        for (int numArmesAfegides = 0; numArmesAfegides < NUM_ARMES; numArmesAfegides++) {
            int nivell = (int) (Math.random() * temple.getNumNivells());
            int numCambra = (int) (Math.random() * temple.getNumCambres(nivell));
            Cambra h = temple.getCambra(nivell, numCambra);
            int[] cela = obtenirCelaLliure(h);
            int[] posicio = h.getPosicioCela(cela[0], cela[1]);
            Arma arma = new Arma((float) Math.random() * 26);
            arma.setPosicioInicial(posicio[0], posicio[1]);
            h.addActor(arma);
        }
    }

    private int[] obtenirCelaLliure(Cambra cambra) {
        int fila = 0;
        int col = 0;
        int cela[] = null;
        boolean trobada = false;
        boolean terra = false;

        while (!trobada) {
            terra = false;
            while (!terra) {
                fila = (int) Math.max(0, (Math.random() * NUM_CELES_VERTICALS - 2));
                col = (int) Math.max(0, (Math.random() * NUM_CELES_HORIZONTALS - 2));
                terra = (cambra.getValor(fila, col) == SIMBOL_TERRA &&
                        cambra.getValor(fila + 1, col) == SIMBOL_TERRA &&
                        cambra.getValor(fila, col + 1) == SIMBOL_TERRA &&
                        cambra.getValor(fila + 1, col + 1) == SIMBOL_TERRA);
            }

            // comprovar que cap actor esta dins la cela
            Actor[] actors = cambra.getActorsAsArray();
            int i = 0;
            boolean lliure = true;
            while (i < actors.length && lliure) {
                cela = cambra.getCela(actors[i].getPosicioInicial()[0],
                        actors[i].getPosicioInicial()[1]);
                lliure = fila != cela[0] || col != cela[1];
                i++;
            }

            //comprovar que el cindy_jones no esta dins la cela
            if (lliure) {
                cela = cambra.getCela(cindy_jones.getPosicioInicial()[0],
                        cindy_jones.getPosicioInicial()[1]);
                lliure = fila != cela[0] || col != cela[1];
            }

            trobada = lliure;
        }
        return new int[]{fila, col};
    }

}
