package edu.ub.recercaarcaperduda;

import edu.ub.recercaarcaperduda.actors.Arqueologa;

import java.awt.*;

/**
 * Representa el marcador de forca, punts i gemmes encontrades (no implementat)
 *
 * @author ub.edu
 */
public class Marcador {

    private int y = 0;

    private Font fontNormal = new Font("Dialog", Font.PLAIN, 12);
    private Font fontPetita = new Font("Dialog", Font.BOLD, 10);

    public Marcador() {
        y = Constants.ALCADA_FINESTRA - 30;
    }

    /**
     * Dibuixa el seu contingut.
     *
     * @param ctx context del joc
     * @param g   grafics de sortida
     */
    public void render(Context ctx, Graphics2D g) {
        pintarFons(g);
        pintarNivellForca(ctx, g);
        pintarCambra(ctx, g);
        pintarCristals(ctx, g);
        if (ctx.getTempsEnemicInmovilitzat() > 0)
            pintarTimer(ctx, g);
    }


    // private methods *********************************************************

    private void pintarFons(Graphics2D g) {
        g.setPaint(new GradientPaint(
                Constants.AMPLADA_FINESTRA / 2.f, y, Color.DARK_GRAY,
                Constants.AMPLADA_FINESTRA / 2.f, y + 40.f, Color.BLACK));

        Rectangle r = new Rectangle(0, y,
                Constants.AMPLADA_FINESTRA, Constants.ALCADA_FINESTRA - y);
        g.fill(r);
    }

    private void pintarNivellForca(Context ctx, Graphics2D g) {
        g.setPaint(Color.white);
        g.setFont(fontNormal);
        g.drawString("forca: ", 5, y + 15);

        Rectangle r = new Rectangle(40, y + 5, 100, 12);
        g.setColor(Color.BLACK);
        g.fill(r);

        Rectangle t = new Rectangle();
        t.setRect(r.getX(), r.getY(), ctx.getJoc().getArqueologa().getForca(), (int) r.getHeight());

        int nivell = (int) (ctx.getJoc().getArqueologa().getForca());

        g.setColor(nivell < 50 ? Color.RED : Color.BLUE);
        g.fill(t);
        g.setColor(Color.white);
        g.draw(r);
        g.setFont(fontPetita);

        g.drawString(nivell + "%", 60, y + 15);
    }


    private void pintarCambra(Context ctx, Graphics2D g) {
        g.setColor(Color.white);
        g.setFont(fontNormal);
        int Nivell = ctx.getJoc().getTemple().getNivell();
        int cambra = ctx.getJoc().getTemple().getNumCambra();
        g.drawString("Nivell: " + Nivell + " - Cambra: " + cambra, 200, y + 15);
    }

    private void pintarCristals(Context ctx, Graphics2D g) {
        g.setColor(Color.white);
        g.setFont(fontNormal);
        Arqueologa arqueologa = (Arqueologa) ctx.getJoc().getArqueologa();
        g.drawString("Num Cristals de Cofre: " + arqueologa.getNumClaus(), 350, y + 15);
    }

    private void pintarTimer(Context ctx, Graphics2D g) {
        g.setColor(Color.white);
        g.setFont(fontNormal);
        Arqueologa arqueologa = (Arqueologa) ctx.getJoc().getArqueologa();
        g.drawString("Enemic immobilitzat!: " + ctx.getTempsEnemicInmovilitzat(), 550, y + 15);
    }
}
