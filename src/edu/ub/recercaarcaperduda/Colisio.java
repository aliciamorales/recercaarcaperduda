package edu.ub.recercaarcaperduda;

import edu.ub.recercaarcaperduda.actors.Actor;

/**
 * Les dades d'una collisio.
 *
 * @author
 */
public class Colisio {

    private Actor actor;

    /**
     * Nova collisio.
     *
     * @param actor l'actor amb qui es collisiona.
     */
    public Colisio(Actor actor) {
        this.actor = actor;
    }

    /**
     * L'actor amb qui es collisiona.
     *
     * @return l'actor
     */
    public Actor getActor() {
        return actor;
    }


}
