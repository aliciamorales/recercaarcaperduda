package edu.ub.recercaarcaperduda.actors;

import edu.ub.recercaarcaperduda.*;

import java.awt.*;

/**
 *
 *
 * @author ub.edu
 */
public class Arma extends AbstractActor {

    private static final int AMPLADA = 50;
    private static final int ALCADA = 59;
    private Image image;



    public Arma(float potencia) {
        this.forca =potencia;
        init();
    }

    public void actualitzar(Context context) {
    }

    public Rectangle getLimits() {
        return new Rectangle(x, y, AMPLADA, ALCADA);
    }

    public void tractarColisio(Colisio colisio) {
        Actor actor = colisio.getActor();
        if (actor instanceof Arqueologa) {
            Arqueologa arqueologa = (Arqueologa) actor;
            arqueologa.addArma(this);
            setEstat(Constants.ESTAT_INACTIU);
        }
    }

    public int[] generarCombinacio() {
        int[] combinacio = new int[4];
        for (int i = 0; i < combinacio.length; i++)
            combinacio[i] = (int) (Math.random() * 10);
        return combinacio;
    }

    public void render(Graphics2D g) {
        if (getEstat() == Constants.ESTAT_ACTIU) {
            g.drawImage(image, x, y, observer);
        }
    }

    private void init() {
        if(forca>=20){
            image = Util.carregarImatge("pistola.png");
        }else if (forca<20 && forca>=10){
            image = Util.carregarImatge("corda.png");
        }else if(forca<10){
            image = Util.carregarImatge("fletxa.png");
        }


    }
}
