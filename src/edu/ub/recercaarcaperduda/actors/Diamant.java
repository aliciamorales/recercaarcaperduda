package edu.ub.recercaarcaperduda.actors;

import edu.ub.recercaarcaperduda.*;
import edu.ub.recercaarcaperduda.Constants.COLORS_DIAMANTS;

import java.awt.*;

/**
 * El oxigen proporciona un increment en el nivell de forca de l'actor que
 * col.lisiona amb ella.
 */
public class Diamant extends AbstractActor {

    private float quantitat;
    private Image image;
    private final int amplada = 20;
    private final int alcada = 37;
    private COLORS_DIAMANTS color;


    /**
     * <code>oxigen.png</code>,  i la quantitat proporcionada. La quantitat de oxigen
     * aporten un guany proporcional en la forca del jugador.
     */
    public Diamant(COLORS_DIAMANTS colorDiamant) {

        color = colorDiamant;

        switch (this.color){
            case VERMELL:
                image = Util.carregarImatge("Diamond.png", new Color(255, 0, 0, 0));
                break;
            case BLAU:
                image = Util.carregarImatge("Diamond.png", new Color(0, 0, 255, 0));
                break;
            case VERD:
                image = Util.carregarImatge("Diamond.png", new Color(0, 255, 0, 0));
                break;
            case GROC:
                image = Util.carregarImatge("Diamond.png", new Color(255, 255, 0, 0));
                break;
            case TARONJA:
                image = Util.carregarImatge("Diamond.png", new Color(255, 128, 64, 0));
                break;
        }
        forca = this.color.getForca();

        this.setEstat(Constants.ESTAT_ACTIU);
    }

    public COLORS_DIAMANTS getColor() {
        return color;
    }
    public void setColor(COLORS_DIAMANTS color){
        this.color = color;
    }

    public float getQuantitat() {
        return quantitat;
    }


    public void setQuantitat(float quantitat) {
        this.quantitat = quantitat;
    }

    public void actualitzar(Context context) {
        // a cada iteracio del joc es crida a actualizar desde la classe GuiJoc al metode actualizarJoc
        // no fem res, es estatic (no se mou)
    }

    public Rectangle getLimits() {
        // es important per tractar les colisions des de la classe GuiJoc al metode actualizarJoc
        return new Rectangle(getX(), getY(), amplada, alcada);
    }

    public void tractarColisio(Colisio colisio) {
        // a cada iteracio del joc es crida a actualizar desde la classe GuiJoc al metode actualizarJoc
        Actor actor = colisio.getActor();
        // Cal explicar el que es instanceof
        if (actor instanceof Arqueologa) {
            actor.setForca(Math.min(100.0f, actor.getForca() + getColor().getForca()));
            ((Arqueologa) actor).addDiamant(this);
            setEstat(Constants.ESTAT_INACTIU);
        }
    }

    public void render(Graphics2D g) {
        //Com dibuixar a la pantalla principal
        g.drawImage(image, getX(), getY(), observer);
    }

}
