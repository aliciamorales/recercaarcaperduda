package edu.ub.recercaarcaperduda.actors;

import edu.ub.recercaarcaperduda.*;

import java.awt.*;

/**
 * Diamants que ha de trobar el jugador.
 *
 * @author ub.edu
 */
public class Cofre extends AbstractActor {

    private static final int AMPLADA = 40;
    private static final int ALCADA = 34;
    private Image image;

    int contingut; // 0: bomba, 1: clau, 2: arca

    public Cofre(int contingut) {
        this.contingut = contingut;
        this.image = Util.carregarImatge("cofre.png");

        this.setEstat(Constants.ESTAT_ACTIU);
    }

    public void actualitzar(Context context) {
    }

    public Rectangle getLimits() {
        return new Rectangle(this.x, this.y, Cofre.AMPLADA, Cofre.ALCADA);
    }

    public void tractarColisio(Colisio colisio) {
        Actor actor = colisio.getActor();
        if (actor instanceof Arqueologa) {
            Arqueologa arqueologa = (Arqueologa) actor;
            switch(contingut){
                case 0:
                    arqueologa.setForca(arqueologa.getForca()/2.f);
                    this.setEstat(Constants.ESTAT_INACTIU);
                    break;
                case 1:
                    if(this.getEstat() == Constants.ESTAT_OBERT){
                        int[] clau = generarCombinacio();
                        arqueologa.addClau(clau);
                        setEstat(Constants.ESTAT_INACTIU);
                    }else{
                        setEstat(Constants.ESTAT_OBERT);
                        image = Util.carregarImatge("key.png");
                    }
                    break;
                case 2:
                    if(this.getEstat() == Constants.ESTAT_OBERT){
                        arqueologa.obrirArcaPerduda();
                    }else{
                        setEstat(Constants.ESTAT_OBERT);
                        image = Util.carregarImatge("arca.png");
                    }
                    break;
            }
        }
    }

    public int[] generarCombinacio() {
        int[] combinacio = new int[4];
        for (int i = 0; i < combinacio.length; i++)
            combinacio[i] = (int) (Math.random() * 10);
        return combinacio;
    }

    public void render(Graphics2D g) {
        if (getEstat() == Constants.ESTAT_ACTIU) {
            g.drawImage(image, x, y, observer);
        }
    }
}
